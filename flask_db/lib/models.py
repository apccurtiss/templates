from dataclasses import dataclass
from datetime import datetime

from flask_sqlalchemy import SQLAlchemy # type: ignore
from safrs import SAFRSBase # type: ignore
from sqlalchemy import DateTime, Text

db = SQLAlchemy()

@dataclass
class User(SAFRSBase, db.Model): #type: ignore
    __tablename__ = 'users'

    http_methods = ['get', 'post', 'patch']
    username: str = db.Column(Text, primary_key=True, nullable=False)
    created: datetime = db.Column(DateTime, nullable=False)