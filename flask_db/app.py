from datetime import datetime
import logging
import sys
from typing import Tuple, Union

import click
from flask import Flask, render_template
from flask_assets import Environment, Bundle # type: ignore
from safrs import SAFRSAPI # type: ignore
from werkzeug.wrappers import Response

from lib.models import db, User

View = Union[Response, str, Tuple[str, int]]

# Init app
app = Flask(__name__)
app.secret_key = 'asdf'

# Init logging
log = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.INFO)

# Init DB
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db.init_app(app)

# Init assets
assets = Environment(app)

style_bundle = Bundle(
    'src/scss/*.scss',
    filters='pyscss',
    output='dist/css/style.css',
    extra={'rel': 'stylesheet/css'}
)

# Register style bundle
assets.register('main_styles', style_bundle)

# Build styles
style_bundle.build()

with app.app_context():
    db.create_all()
    db.session.merge(User(username='TestUser', created=datetime.now()))
    db.session.commit()

    api = SAFRSAPI(app, prefix='/api')
    api.expose_object(
        User,
        # method_decorators=[auth.login_required]
        )

@app.route('/')
def index() -> View:
    users = db.session.query(User).all()
    return render_template('index.html', users=users)

@click.command()
@click.option('--debug', is_flag=True)
@click.option('--port', type=int, default=80)
def main(debug: bool, port: int) -> None:

    log.info('Running on port {}'.format(port))
    app.run('0.0.0.0', threaded=True, debug=debug, port=port)

if __name__ == "__main__":
    main()
